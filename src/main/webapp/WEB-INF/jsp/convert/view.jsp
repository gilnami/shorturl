<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

URL 주소 입력 : <input type="text" size="100" id="txtOrgUrl" name="txtOrgUrl"> <input type="button" onclick="getShortUrl()" value="Short Url 얻기">
<br/>
<br/>
<span id="sp_short"></span>
<br/>
<br/>



Short URL 주소 입력 : <input type="text" size="100" id="txtShortUrl" name="txtShortUrl"> <input type="button" onclick="getOrgUrl()" value="Original Url 얻기">
<br/>
<br/>
<span id="sp_org"></span>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script language="javascript">

function getShortUrl(){
	
	if($("#txtOrgUrl").val() == ""){
		alert("변환할 URL 주소를 입력해주세요");
		$("#txtOrgUrl").focus();
		return false;
	}
	
	$.ajax({
		type : "POST",
        url: "/convert/toShort",
        data: {url : $("#txtOrgUrl").val()},
        success: function (ret) {
        	$("#sp_short").html(ret);
        },
        error: function (e) {
        	
        }
    });
	
}

function getOrgUrl(){


	if($("#txtShortUrl").val() == ""){
		alert("변환할 URL 주소를 입력해주세요");
		$("#txtShortUrl").focus();
		return false;
	}
	
	$.ajax({
		type : "POST",
        url: "/convert/toOrg",
        data: {url : $("#txtShortUrl").val()},
        success: function (ret) {
        	if(ret == ""){
        		$("#sp_org").html("저장된 short url이 없습니다.");	
        	}else{
        		$("#sp_org").html(ret);
        	}
        },
        error: function (e) {
        	
        }
    });
}


</script>