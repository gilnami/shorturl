package shorten.url;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"shorten.url"})
@EnableAutoConfiguration
public class MainApplication {
	public void run(String[] args) throws Exception {
		SpringApplication.run(MainApplication.class, args);
	}

	public static void main(String[] args) throws Exception {
		new MainApplication().run(args);
	}
	
}