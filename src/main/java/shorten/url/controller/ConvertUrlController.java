package shorten.url.controller;

import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import shorten.url.service.ConvertService;

@Controller
@RequestMapping("/convert")
public class ConvertUrlController {

	@Autowired
	private ConvertService convertService;
	
	
	@RequestMapping(value = "/view")
	public String form() throws Exception {
		return "/convert/view";
	}

	/**
	 * 입력받은 주소 Short URL 얻기 
	 * @param url - original url
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/toShort")
	public String convertToShort(@RequestParam(value="url") String url) throws Exception {
		String serverHost = InetAddress.getLocalHost().getHostAddress();
		String convertUrl = serverHost + "/" + convertService.getShortUrl(url.trim());
		return convertUrl;
	}

	/**
	 * 입력받은 short url로 original url 얻기 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/toOrg")
	public String convertToOrg(@RequestParam(value="url") String url) throws Exception {
		String serverHost = InetAddress.getLocalHost().getHostAddress();
		url = url.trim().replaceAll(serverHost+"/", "").replaceAll("http://", "").replaceAll("https://", "");
		String convertUrl = convertService.getOrgUrl(url);
		return convertUrl;
	}

}