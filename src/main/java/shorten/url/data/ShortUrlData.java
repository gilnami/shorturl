package shorten.url.data;

import java.util.HashMap;

/**
 * 발행된 Url 정보 static memory에 저장
 * 
 */
public class ShortUrlData {
	
	//<url sequence number, original url> - shorten url 발행 맵
	public static HashMap<Integer, String> SEQ_KEY_DATA = new HashMap<Integer, String>();
	//<original url, url sequence number> - 이미 shorten url 생성된적이 있는지 검색을 위한 맵
	public static HashMap<String, Integer> URL_KEY_DATA = new HashMap<String, Integer>();
	//url sequence number
	public static int URL_SEQ = 0;

	/**
	 * 데이터 푸시
	 * @param url - original url
	 * @return
	 */
	public static int putData(String url) {
		
		if(URL_KEY_DATA.containsKey(url)){
			return URL_KEY_DATA.get(url);
		}
		
		synchronized(SEQ_KEY_DATA){
			int seq = URL_SEQ;
			SEQ_KEY_DATA.put(seq, url);
			URL_KEY_DATA.put(url, seq);
			URL_SEQ++;
			return seq;
		}
	}
	
	/**
	 * 데이터 가져오기
	 * @param key - sequence number
	 * @return
	 */
	public static String getData(int key) {
		return SEQ_KEY_DATA.get(key);
	}
}
