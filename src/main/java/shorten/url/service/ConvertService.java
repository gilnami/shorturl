package shorten.url.service;

import org.springframework.stereotype.Service;

import shorten.url.data.ShortUrlData;

@Service
public class ConvertService {

	//url 생성할 기본 문자열
	private final String ShortenBaseStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	//url 생성할 기본 문자열 총 갯수. n진수로 변환하기 위함
	private final int ShortenBase = ShortenBaseStr.length();
	
	/**
	 * Orginal Url -> Short Url
	 * @param orgUrl
	 * @return
	 * @throws Exception
	 */
	public String getShortUrl(String orgUrl) throws Exception{
		int seq = ShortUrlData.putData(orgUrl);
		return toShortenUrlFromSeq(seq);
	}
	
	/**
	 * Short Url -> Original Url
	 * @param shortUrl
	 * @return
	 * @throws Exception
	 */
	public String getOrgUrl(String shortUrl) throws Exception{
		int seq = toSeqFromShortenUrl(shortUrl);
		return ShortUrlData.getData(seq);
	}
	
	
	//url의 10진수로 기록된 sequence 번호를 n진수로 변환
	private String toShortenUrlFromSeq(int decimalNum) {
		StringBuilder sb = new StringBuilder();
		do {
			sb.append(ShortenBaseStr.charAt(decimalNum % ShortenBase));
			decimalNum /= ShortenBase;
		} while (decimalNum > 0);
		return sb.reverse().toString();
	}

	//n진수의 shorturl을 10진수 sequence로 변환
	private int toSeqFromShortenUrl(String shortUrl) {
		int decimalNum = 0;
		for (int i=0; i<shortUrl.length(); i++) {
			decimalNum = (decimalNum * ShortenBase) + ShortenBaseStr.indexOf(shortUrl.charAt(i));
		}
		return decimalNum;
	}

}
