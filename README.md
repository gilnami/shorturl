#MainApplication 실행 옵션 Argument
 -Dspring.config.location="file:config/localhost/application.yml"

#URL 변환 화면 출력 
 - http://localhost:9090/convert/view
 
 
#구현내용
  - URL 입력받을 경우 Short Url로 변환출력 (동일한 url이 존재할 경우, 동일한 short url 전달)
  - Short Url 입력 받을 경우, Original URL 출력
  - 요청된 URL데이터는 static 메모리 저장(재시작시, 기존 저장된 내용 사라짐)  
  - 최대 Max.Integer 만큼 생성 가능
  
#알고리즘
  - Short Url을 중복되지 않도록 매번 생성하는 방법에 대해서 고민
  - 요청된 Url마다 sequence number를 발행하고, 그 number를 문자형으로 변환하는 형태로 중복검색이 필요 없는 빠른 키 발행을 목표로 함.
  - 결국 10진수를 [a-zA-Z0-9]로 이루어진 62진 형태로 변환
